#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "4157 9702\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 4157)
        self.assertEqual(j, 9702)

    def test_read_3(self):
        s = "83252 54879\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 83252)
        self.assertEqual(j, 54879)

    def test_read_4(self):
        s = "756 756\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 756)
        self.assertEqual(j, 756)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(4157, 9702)
        self.assertEqual(v, 262)

    def test_eval_3(self):
        v = collatz_eval(83252, 54879)
        self.assertEqual(v, 351)

    def test_eval_4(self):
        v = collatz_eval(756, 756)
        self.assertEqual(v, 109)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 4157, 9702, 262)
        self.assertEqual(w.getvalue(), "4157 9702 262\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 83252, 54879, 351)
        self.assertEqual(w.getvalue(), "83252 54879 351\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 756, 756, 109)
        self.assertEqual(w.getvalue(), "756 756 109\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 999 174\n")

    def test_solve_2(self):
        r = StringIO("8477 3858\n7143 7361\n4157 9702\n5042 4624\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8477 3858 262\n7143 7361 195\n4157 9702 262\n5042 4624 210\n")

    def test_solve_3(self):
        r = StringIO("106 682\n35 13\n831 306\n33 781\n242 50\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "106 682 145\n35 13 112\n831 306 171\n33 781 171\n242 50 128\n")

    def test_solve_4(self):
        r = StringIO("15262 77745\n24255 25020\n71598 94195\n53706 30377\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "15262 77745 351\n24255 25020 264\n71598 94195 351\n53706 30377 340\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
